import React from 'react';
import ReactDOM from 'react-dom';

class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <nav class="uk-navbar-container uk-margin" uk-navbar>
          <div class="uk-navbar-center">
      
              <div class="uk-navbar-center-left"><div>
                  <ul class="uk-navbar-nav">
                      <li class="uk-active"><a href="#">Active</a></li>
                      <li>
                          <a href="#">Parent</a>
                          <div class="uk-navbar-dropdown">
                              <ul class="uk-nav uk-navbar-dropdown-nav">
                                  <li class="uk-active"><a href="#">Active</a></li>
                                  <li><a href="#">Item</a></li>
                                  <li><a href="#">Item</a></li>
                              </ul>
                          </div>
                      </li>
                  </ul>
              </div></div>
              <a class="uk-navbar-item uk-logo" href="#">Logo</a>
              <div class="uk-navbar-center-right"><div>
                  <ul class="uk-navbar-nav">
                      <li><a href="#">Item</a></li>
                  </ul>
              </div></div>
      
          </div>
      </nav>
    )
  }
}

export default Navbar;