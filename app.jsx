import React from 'react';
import ReactDOM from 'react-dom';
import Navbar from './components/navbar.jsx';
import TopFive from './components/topfive.jsx';


class Title extends React.Component {
  render() {
    return (<h1 className="uk-text-lead">{this.props.label}</h1>)
  }
}

class Paragraph extends React.Component {
  render() {
    return (
      <p className="uk-text-meta">
      {this.props.text}
      </p>
    )
  }
}

class Main extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render() {
    return  (
      <div>
    
        <Navbar />
        
        <div className="uk-container uk-text-center">
          <Title label={this.props.title} />
          <Paragraph text={this.props.text} />
          
          <TopFive />
        </div>
      </div>
    )
  }
}


ReactDOM.render(
  <Main title="React + UIkit" text="Time flies like an arrow, fruit flies like a banana"></Main>,
  document.getElementById('react-app')
);
